!include "StrFunc.nsh"

;${StrRep}
;${UnStrRep}
 
!define SPECIAL_CHAR "!*'();:@&=+,/?#[]%\$$"
!define SPECIAL_CODE "%21%2A%27%28%29%3B%3A%40%26%3D%2B%24%2C%2F%3F%23%5B%5D%25"
 
!define UrlEncode  `!insertmacro UrlEncode_Call`
!define UnUrlEncode `!insertmacro UnUrlEncode_Call`

!macro UrlEncode_Call ResultVar String
    Push `${String}`
    Call UrlEncode
    Pop `${ResultVar}`
!macroend

!macro UnUrlEncode_Call ResultVar String
    Push `${String}`
    Call un.UrlEncode
    Pop `${ResultVar}`
!macroend
 
!macro UrlEncode UN
Function ${UN}UrlEncode
	Exch $R0
	push $R1
	push $R2
	push $R3
	push $R4
	push $R5
	 
	StrLen $R1 ${SPECIAL_CHAR}
	IntOp  $R2 0 + 0

	loop:
	;Get the special char value
	StrCpy $R3 ${SPECIAL_CHAR} 1 $R2
	;Get the special char code
	StrCpy $R4 ${SPECIAL_CODE} 3 $R5

	;replace the special char occurence with its code
	!ifdef __UNINSTALL__
		${UnStrRep} $R0 "$R0" "$R3" "$R4"
	!else
		${StrRep} $R0 "$R0" "$R3" "$R4"
	!endif

	;Increment special charater code index
	IntOp  $R5 $R5 + 3
	;Increment loop counter
	IntOp  $R2 $R2 + 1
	IntCmp $R2 $R1 +2 loop +2
	goto loop
			
	pop  $R5
	pop  $R3
	pop  $R2
	pop  $R1
	Exch $R0
FunctionEnd
!macroend
!insertmacro UrlEncode ""
!insertmacro UrlEncode "un."
