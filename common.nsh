;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GetLocalAppData Definition Start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
!define	GetLocalAppData "!insertmacro GetLocalAppData"
!macro GetLocalAppData _OUTPUT
	Call GetLocalAppData
	Pop ${_OUTPUT}
!macroend

Function GetLocalAppData
	Push $0
	${UserReadRegStr} $0 'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders' 'Local AppData'
	Exch $0
FunctionEnd
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GetLocalAppData Definition End
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;--------------------------------
; Init the global params
;

!macro initParams UN
Function ${UN}initParams
	;--------- Initialisation ----
	StrCpy $EnableNewTab	false
	StrCpy $NewTabSource	""
	StrCpy $theUser			""
	StrCpy $theUserDir		""
	StrCpy $theUserDirAdmin	""
	StrCpy $theUserReg		""
	StrCpy $theUserRegFull	""
	StrCpy $sCountryRegionCode	""

;MessageBox MB_OK "INSTDIR=$INSTDIR"	

	${If} "$INSTDIR" == ""
		!ifdef DefaultProgramFiles
			StrCpy $ieToolbarInstallFolder "$PROGRAMFILES\${DefaultProgramFiles}"
		!else
			StrCpy $ieToolbarInstallFolder "$PROGRAMFILES\${AppFolderName}"
		!endif
	${Else}
		StrCpy $ieToolbarInstallFolder "$INSTDIR"
	${EndIf}
	; TODO: Remove trailing backslash from $ieToolbarInstallFolder, if any.
	
;MessageBox MB_OK "ieToolbarInstallFolder=$ieToolbarInstallFolder"
	
	; Let's use FF Extension ID as it is unique
	StrCpy $UninstallerElevationPolicyID ${FirefoxExtCLSID}

    ;-------------------
	${If} ${AtLeastWinVista}
		ReadEnvStr $9 USERPROFILE
		StrCpy $theUserDirAdmin "$9\AppData\LocalLow"
		StrCpy $theUserDir $theUserDirAdmin
		StrCpy $theUserRegFull 'Software\AppDataLow\Software\'

		StrCpy $ieToolbarCacheFolder "$theUserDir\${AppFolderName}"
		
		;At installation; we do not care which is the proper "toolbar local", as at the very end we will copy locallow folder to EPM folder. However at uninstallation with ; we need to get the proper folder to read some files properly such as uninstallIE.dat
		!ifdef __UNINSTALL__
			ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion		
			${VersionCompare} "6.2" "$R0" $8
			${if} "$8" != 1
				ReadRegStr $7 HKCU "SOFTWARE\\Microsoft\\Internet Explorer\\Main" "isolation"
				${if} "$7" == "" 
				${OrIf} "$7" == "PMEM"
					Call ${UN}GetEPMContainerFolder
					Pop $6
					StrCpy $ieToolbarCacheFolder $6
				${endif}
			${endif}				
		!endif
	${Else}
		StrCpy $theUserDirAdmin "$APPDATA"
		StrCpy $theUserDir $theUserDirAdmin
		StrCpy $theUserRegFull 'Software'
		
		StrCpy $ieToolbarCacheFolder "$APPDATA\${AppFolderName}"
	${EndIf}
	
	Call ${UN}getFirefoxProfileFolders
FunctionEnd
!macroend   ; initParams UN
!insertmacro initParams ""
!insertmacro initParams "un."
