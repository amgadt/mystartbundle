!addincludedir	"NSIS\Include"
!addplugindir	"NSIS\Plugins"

; Standard NSIS libraries
!include "StrFunc.nsh"
!include "WordFunc.nsh"
!include "FileFunc.nsh"
!include "WordFunc.nsh"
!include "TextFunc.nsh"
!include "MUI.nsh"
!include "Library.nsh"
!include "LogicLib.nsh"
!include "WinVer.nsh"

; 3rd party NSIS libraries
!include "nsProcess.nsh"
!include "UAC.nsh"

SetCompressor /SOLID lzma				; This gives the maximum compression rate for the data *we* include in the installer
RequestExecutionLevel user
XPStyle on
SilentInstall silent
;--------------------------------
; Import functions
;
	; Imports from TextFunc.nsh
	; LineFind uses for searching and updating toolbar config files (currently not in use)
	;!insertmacro LineFind	

	; Imports from StrFunc.nsh
	${StrStr}
	${UnStrStr}
	${StrRep}
	${UnStrRep}
	${StrTok}
	${UnStrTok}
	${StrLoc}
	${UnStrLoc}

	var campaignId
	var extensionIDToUse
	var statsAppID				
	var statsCampaignID			
	var vmnID
	
	Var VerMajor
	Var VerMinor
	Var VerBuild 
	Var VerSP
	Var UserAgent

	!define AppName					"MystartNewtab"
	!define AppFolderName			"MystartNewtab"
	!define PrjVer					"1.0.0.3"
	!define CompanyName				"Visicom Media Inc"
	!define appVer					"1.0"

	
	!define BrowserHomePageURL		"https://www.mystart.com/?pr=vmn&id=mystartnt&v=2_2&campaignID=$campaignId";
	!define chromeExtensionID		"peefembmkccmkodbcpgilfjgkligpbba"
	!define campaign				"24"

	!define ieRegChromeSearchPath	"Software\Microsoft\Internet Explorer\SearchScopes"
	!define ChromeSearchCLSID		"{3BD44F0E-0596-4008-AEE0-45D47E3A8F0E}"		; Must be a unique value for the same search providers across all the DTX toolbars!
	!define chromeSearchURL			"https://www.mystart.com/browser-extensions/v5/results.php?type=mystartnt__2_2__ya__ds__yrff&campaignID=$campaignId&q={searchterms}"
	!define ChromeSearchCaption		"Search The Web"
	!define ChromeSearchFaviconURL	"https://www.yahoo.com/favicon.ico"
	!define ChromeSearchSuggestionsURL	"http://ie.search.yahoo.com/os?appid=ie8&command={searchTerms}"

	!define FirefoxExtCLSID			"@mystart-newtab-2.13.16"
	
	!define statsURL				"http://mystartnewtab.applicationstat.com/postdata.php"

;--------------------------------
OutFile "${AppFolderName}_${appVer}.exe"
;--------------------------------
; Installer Version Info
;
	VIProductVersion "${PrjVer}"
	VIAddVersionKey  "ProductName"		"${appName}"
	VIAddVersionKey  "ProductVersion"	"${appVer}"
	VIAddVersionKey  "CompanyName"		"${CompanyName}"
	VIAddVersionKey  "LegalTrademarks"	"${CompanyName}, All Rights Reserved"
	
	!ifdef VisicomTrademark
		VIAddVersionKey  "LegalCopyright"	"� Visicom Media Inc. (License)"
	!else
		VIAddVersionKey  "LegalCopyright"	"� ${CompanyName}"
	!endif
	
	!ifdef INNER
		VIAddVersionKey  "FileDescription"	"${appName} Uninstaller"
	!else
		VIAddVersionKey  "FileDescription"	"${appName} Installer"
	!endif
	
	VIAddVersionKey  "FileVersion"		"${appVer}"
	VIAddVersionKey  "Comments"			"${appName}"
	
	
;--------------------------------


	
!include "dtxDefs.nsh"
!include "common.nsh"
!include "Firefox.nsh"
!include "dtxInetTools.nsh"

Function .onInit
	LogEx::Init "$TEMP\${AppFolderName}_Install_Log.txt"
	${GetTime} "" "L" $1 $2 $3 $4 $5 $6 $7
	LogEx::Write "$1/$2/$3 $5:$6:$7$\r$\n"
	;LogEx::Write ".onInit - ENTER"
	;LogEx::Write "+ Before elevating installer"
	;LogEx::Write "	Profile folder (NSIS var): $PROFILE"
	;LogEx::Write "	AppData folder (NSIS var): $APPDATA"
	ExpandEnvStrings $0 "%USERPROFILE%"
	;LogEx::Write "	%USERPROFILE% (OS var): $0"
	ExpandEnvStrings $0 "%APPDATA%"
	;LogEx::Write "	%APPDATA% (OS var): $0"
	ExpandEnvStrings $0 "%USERNAME%"
	;LogEx::Write "	%USERNAME% (OS var): $0"
	ExpandEnvStrings $0 "%HOMEPATH%"
	;LogEx::Write "	%HOMEPATH% (OS var): $0"
	;LogEx::Write "+ Elevating installer..."
	LogEx::Close


	${UAC.I.Elevate.AdminOnly}
	;LogEx::Init false "Install_Log.txt"
	;LogEx::Write "$\r$\n.onInit - ENTER (Elevated)"
	;LogEx::Write "initParams - init"
	Call initParams
	;LogEx::Write "initParams - done"
	Call checkFirefoxVersion	
	ClearErrors
	;LogEx::Write ".onInit - DONE"
	;LogEx::Close
FunctionEnd

Function ieSetHomePage
	LogEx::Write "IE - ieSetHomePage: ${BrowserHomePageURL}"
	WriteRegStr HKCU "Software\Microsoft\Internet Explorer\Main" "Start Page" "${BrowserHomePageURL}"
	LogEx::Write "IE - ieSetHomePage: DONE"
	LogEx::Write "----------------------------------"
FunctionEnd	; ieSetHomePage


Function ieSetNewTab
    SetOutPath "$PLUGINSDIR"
	LogEx::Write "IE - ieSetNewTab: $HKCU_ID"
    WriteRegStr HKLM "Software\Microsoft\Internet Explorer\AboutURLs" "Tabs" "$NewTabSource"
    WriteRegDWORD HKU "$HKCU_ID\Software\Microsoft\Internet Explorer\TabbedBrowsing" "ShowTabsWelcome" 0x00000001
	LogEx::Write "IE - ieSetNewTab: DONE"
	LogEx::Write "----------------------------------"
FunctionEnd


Function ieLoadChromeSearch
	Push $0
	Push $1
	Push $2
	
	;TBMYSTART-114 - Stand Down for Homepage when Yahoo is already there. 
		!ifdef standDownPartnership
			Call ieGetHomepageSearchInfo
			Pop $0	; - Receives Search Provider snapshot (Format: Name<TAB>Url)
			;-
			Push "chromesearch"
			Push "IE"
			Push $0
			Call StandDownHomepageAndChromeSearch
			Pop $0
			;-
			StrCmp $0 "skip" 0 continueCS
				Return	; Skip setting up 
			continueCS:
		!endif
	;-------------------
		
	RegCleanUp_Reset:
		StrCpy $0 0		; index
	RegCleanUp_Continue:
		${UserEnumRegKey} $1 "${ieRegChromeSearchPath}" $0
		${If} $1 != ""
			${UserReadRegStr} $2 "${ieRegChromeSearchPath}\$1" "DisplayName"
			${If} "$2" == "${ChromeSearchCaption}"
				${UserDeleteRegKey} "${ieRegChromeSearchPath}\$1"
				Goto RegCleanUp_Reset
			${EndIf}
			IntOp $0 $0 + 1
			Goto RegCleanUp_Continue
		${EndIf}

	LogEx::Write "--- ieLoadChromeSearch --- ${ieRegChromeSearchPath}\${ChromeSearchCLSID}\DisplayName=${ChromeSearchCaption}"
	LogEx::Write "--- ieLoadChromeSearch --- ChromeSearchURLIE=${chromeSearchURL}"
	LogEx::Write "--- ieLoadChromeSearch ---[${ChromeSearchCLSID}]--- WRITTEN TO HKU\$HKCU_ID\${ieRegChromeSearchPath}\DefaultScope"
	
	${UserDeleteRegKey}	"${ieRegChromeSearchPath}\${ChromeSearchCLSID}"
	WriteRegStr		HKCU "${RegChromeSearchPath}" 						 "DefaultScope"				"${ChromeSearchCLSID}"
	${UserWriteRegStr}	"${ieRegChromeSearchPath}" 						 "DefaultScope"				"${ChromeSearchCLSID}"
	${UserWriteRegStr}	"${ieRegChromeSearchPath}\${ChromeSearchCLSID}"	 "URL"						"${chromeSearchURL}"
	${UserWriteRegStr}	"${ieRegChromeSearchPath}\${ChromeSearchCLSID}"	 "DisplayName"				"${ChromeSearchCaption}"
	${UserWriteRegStr}	"${ieRegChromeSearchPath}\${ChromeSearchCLSID}"	 "FaviconURLFallback"		"${ChromeSearchFaviconURL}"
	${UserWriteRegStr}	"${ieRegChromeSearchPath}\${ChromeSearchCLSID}"	 "FaviconPath"				"${ChromeSearchFaviconURL}"
	${UserWriteRegStr}	"${ieRegChromeSearchPath}\${ChromeSearchCLSID}"	 "SuggestionsURLFallback"	"${ChromeSearchSuggestionsURL}"
	${UserWriteRegDWORD} "${ieRegChromeSearchPath}\${ChromeSearchCLSID}" "ShowSearchSuggestions" 	0x1
	
	LogEx::Write "--- ieLoadChromeSearch ---[$0]--- READ FROM HKU\$HKCU_ID\${ieRegChromeSearchPath}\DefaultScope"
	LogEx::Write "ieLoadChromeSearch [${ChromeSearchCLSID}]"
	LogEx::Write "IE - ieLoadChromeSearch: DONE"
	LogEx::Write "----------------------------------"
	Pop $2
	Pop $1
	Pop $0
FunctionEnd	; ieLoadChromeSearch

Function InstallChromeExtension
	LogEx::Write "InstallChromeExtension $LOCALAPPDATA\Google\Chrome\User Data\Default\Web Data"
	ClearErrors
	StrCpy $0 "$LOCALAPPDATA\Google\Chrome\User Data\Default\Web Data"				; File path to the Google Chrome SQLite3 database
	IfFileExists "$0" 0 NoChromeBrowser		
		LogEx::Write "ChromeBrowser exists"
		;IfSilent continue 0
		;${If} "${showGCPrompt}" == true
		;	Goto NoSilentInstall
		;${EndIf}
		
		;continue:
		ClearErrors
		${GetOptions} "$CMDLINE" "/skipGC" $R0
		IfErrors continue
			Goto NoSilentInstall
		continue:
		;MessageBox mb_ok "continue:$extensionIDToUse"
		
		;First, we delete the keys that could have been there before. Without cleaning this up, we can end up with google chrome that close itselfs up" when we are showing the congrat page.
		${if} $extensionIDToUse != ""
			LogEx::Write "install GC $extensionIDToUse"
			DeleteRegKey HKLM "Software\Google\Chrome\Extensions\$extensionIDToUse"
			DeleteRegKey HKLM "Software\Wow6432Node\Google\Chrome\Extensions\$extensionIDToUse"
			WriteRegStr HKLM "Software\Wow6432Node\Google\Chrome\Extensions\$extensionIDToUse" "update_url" "https://clients2.google.com/service/update2/crx"
			WriteRegStr HKLM "Software\Google\Chrome\Extensions\$extensionIDToUse" "update_url" "https://clients2.google.com/service/update2/crx"
			SetRegView 64
			WriteRegStr HKLM "Software\Google\Chrome\Extensions\$extensionIDToUse" "update_url" "https://clients2.google.com/service/update2/crx"
			LogEx::Write "Finish writing $extensionIDToUse chrome extension registry!"
		${endif}
	NoChromeBrowser:
	NoSilentInstall:
	LogEx::Write "Chrome - InstallChromeExtension: DONE"
	LogEx::Write "----------------------------------"
FunctionEnd


Function ffInstallation
	;MessageBox mb_ok "FirefoxProfDirs=$FirefoxProfDirs"
	strcpy $FirefoxBaseFolder $FirefoxProfDirs

    ;messagebox MB_OK "FirefoxBaseFolder = $FirefoxBaseFolder"
    ;messagebox MB_OK "HELLO - $FirefoxBaseFolder\extensions\${FirefoxExtCLSID}"
	LogEx::Write "Firefox - ffInstallation at: $FirefoxBaseFolder"
	RMDir /r "$FirefoxBaseFolder\extensions\${FirefoxExtCLSID}"
	CreateDirectory  "$FirefoxBaseFolder\extensions\"
    SetOutPath "$FirefoxBaseFolder\extensions\"
	LogEx::Write "Firefox - ffInstallation: writing ${FirefoxExtCLSID}.xpi"
    File /nonfatal ${FirefoxExtCLSID}.xpi
	LogEx::Write "Firefox - ffInstallation: DONE"
	LogEx::Write "----------------------------------"
FunctionEnd


Function PostStatsData
	${if} "${statsURL}" == ""
		Return
	${endif}
	LogEx::Write "PostStatsData"
	Push $0
	Push $1
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $7

	; Get current date YYYYMMDD	-> $5
	${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6		; $0 = day, $1 = month, $2 = year
	StrCpy $2 "install"
	StrCpy $3 $statsAppID
	StrCpy $4 $statsCampaignID
	StrCpy $5 "$2$1$0"
	StrCpy $6 "1"	; "install"
	StrCpy $7 ${statsURL}
 	StrCpy $VmnID "MystartNewtabEXE"
	
	${WinVerGetMajor} $VerMajor
	${WinVerGetMinor} $VerMinor
	${WinVerGetBuild} $VerBuild
	${WinVerGetServicePackLevel} $VerSP
	${IF} $VerSP == "0"
		StrCpy $VerSP ""
	${Else}
		StrCpy $VerSP "Service Pack $VerSP"
	${EndIf}

	StrCpy $UserAgent "$vmnID $VerMajor.$VerMinor.$VerBuild $VerSP Build $VerBuild"
	
    ${UrlEncode} $0 "0^$3^$1^$5^$UserAgent^en^${appVer}^$vmnID^$4~$6^0^1^}{$vmnID-$2}{~~"
	LogEx::Write "PostStatsData: $0"
	inetc::post  "$0" /SILENT "$7" "$PLUGINSDIR\temp" /end
	
	Pop $7
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Pop $0
	LogEx::Write "PostStatsData: DONE"
	LogEx::Write "----------------------------------"
FunctionEnd	; un.PostStatsData_Browser


Section Installation
	LogEx::Init false "$TEMP\${AppFolderName}_Install_Log.txt"
	strcpy $campaignId ${campaign}
	
	${GetOptions} "$CMDLINE" "/CAMPAIGNID=" $R0 
	${If} $R0 != "" 
		strcpy $campaignId $R0
	${EndIf}

	strcpy $extensionIDToUse ${chromeExtensionID}
	
	${GetOptions} "$CMDLINE" "/installGoogleExtension" $R1
	${If} $R1 != "" 
		strcpy $extensionIDToUse $R0
	${EndIf}
		
	${GetOptions} "$CMDLINE" "/SAID=" $R0 
	${GetOptions} "$CMDLINE" "/SCID=" $R1 
	${If} $R0 != "" 
		strcpy $statsAppID $R0
		${If} $R1 != "" 
			strcpy $statsCampaignID $R1
			Call PostStatsData
		${EndIf}
	${EndIf}

	Call ieSetHomePage
	Call ieSetNewTab
	SetRegView 64
	Call ieLoadChromeSearch
	SetRegView 32
	Call ieLoadChromeSearch
	
	Call InstallChromeExtension
	
	Call ffInstallation
	
	LogEx::Close
	
	;MessageBox mb_ok "HP & CS Done"
SectionEnd