	
	Var HKCU_ID								; Current user SID for proper accessing HKCU via HKU\$HKCU_ID
											; Some wrapper installers modify HKCU contect and accessing HKCU goes nowhere. 
											; Using HKU\$HKCU_ID directly solves the problem.
	
;--------------------------------
; Global variables for all processes
; Get initialized in initParams
;
	Var ieToolbarInstallFolder				; Toolbar installation folder for IE. ??? Why not to use $INSTDIR ???
	Var ieToolbarCacheFolder				; Toolbar user preferences folder for IE (%APPDATA% [+ LocalLow] + Toolbar folder name)
	Var theUserDir							; User preferences folder for IE (%APPDATA% [+ LocalLow])
	Var tempdirIni          
	Var theUserDirAdmin
	Var theUser
	Var theUserReg
	Var theUserRegFull


;--------------------------------
; Global Variables
;
	Var guid				; Gets initialized by CreateGUID
	Var IsBrowserRunning	; 1 = a browser (ie, ff, chrome) is running
	Var sCountryRegionCode
	Var sCountryYahooRegionCode ; Country 2 letters name compatible with yahoo country search

	; NewTab feature supprt
	Var EnableNewTab
	Var NewTabSource

	; Allows to launch the uninstaller w/o UAC prompt.
	; *NOTE*: It must be a unique value per project!!!
	; Let's set it to FF Extension ID value as it is unique as well.
	Var UninstallerElevationPolicyID

	Var url_onInstalled
	Var url_onUninstalled
	;Var url_HomePage

	;Var uninstallStringIE
	;Var uninstallStringFF
	;Var uninstallStringIEDTX
	;Var uninstallStringFFDTX
	
	Var silentCampaignID
	Var silentApplicationID

	Var CfgHomePageSet
	Var CfgAddressbarSearchSet
	Var CfgChromeSearchSet
	
	Var BrowserHomePageURL
;--------------------------------
; 
;	

	!define	dtxBundlesFolder	"..\..\..\dtx.Bundles\"						; Base folder for additional optional componetns such as 
																			; Network Error Advisor, FF media player, Email Notifier, VMN Antispyware, etc.

	;This new stats URL is used for Wizard DTX Toolbars new system, which work along with the "old dt5-stats" system.
	!define dtxStatsWizardURL	"http://184.73.232.5/post_streams/postdata.php"		; Used by un.PostStatsData


;--------------------------------
;
;
																			
; CreateGUID - Returns a new GUID on the top of the stack
Function CreateGUID
	Push $0
	Push $1
	Push $2

	System::Alloc 80
	System::Alloc 16
	System::Call 'ole32::CoCreateGuid(i sr1) i'
	System::Call 'ole32::StringFromGUID2(i r1, i sr2, i 80) i'
	System::Call 'kernel32::WideCharToMultiByte(i 0, i 0, i r2, \
				i 80, t .r0, i ${NSIS_MAX_STRLEN}, i 0, i 0) i'
	System::Free $1
	System::Free $2

	Pop $2
	Pop $1
	;StrCpy $guid $0
	Exch $0
FunctionEnd


!macro GetCommonAppData UN
Function ${UN}GetCommonAppData
	Push $1
	Push $2
	Push $3
	Push $4  
	 
	StrCpy $1 ""
	StrCpy $2 "0x0023" # CSIDL_COMMON_APPDATA  (0x0023)
	StrCpy $3 ""
	StrCpy $4 ""
	 
	System::Call 'shell32::SHGetSpecialFolderPath(i $HWNDPARENT, t .r1, i r2, i r3) i .r4'
	 
	Pop $4
	Pop $3
	Pop $2
	Exch $1
FunctionEnd
!macroend
!insertmacro GetCommonAppData ""
!insertmacro GetCommonAppData "un."


;--------------------------------
; http://nsis.sourceforge.net/Another_String_Replace_(and_Slash/BackSlash_Converter)
;
; Push $filenamestring (e.g. 'c:\this\and\that\filename.htm')
; Push "\"
; Call StrSlash
; Pop $R0
; ;Now $R0 contains 'c:/this/and/that/filename.htm'
;
!macro StrSlash UN
Function ${UN}StrSlash
	Exch $R3 ; $R3 = needle ("\" or "/")
	Exch
	Exch $R1 ; $R1 = String to replacement in (haystack)
	Push $R2 ; Replaced haystack
	Push $R4 ; $R4 = not $R3 ("/" or "\")
	Push $R6
	Push $R7 ; Scratch reg
	StrCpy $R2 ""
	StrLen $R6 $R1
	StrCpy $R4 "\"
	StrCmp $R3 "/" loop
	StrCpy $R4 "/"  

	loop:
		StrCpy $R7 $R1 1
		StrCpy $R1 $R1 $R6 1
		StrCmp $R7 $R3 found
		StrCpy $R2 "$R2$R7"
		StrCmp $R1 "" done loop
	found:
		StrCpy $R2 "$R2$R4"
		StrCmp $R1 "" done loop
	done:
	
	StrCpy $R3 $R2
	Pop $R7
	Pop $R6
	Pop $R4
	Pop $R2
	Pop $R1
	Exch $R3
FunctionEnd
!macroend
!insertmacro StrSlash ""
!insertmacro StrSlash "un."



;
; Function UserEnumRegKey
; Usage ${UserEnumRegKey} $Result "reg path" "count"
;
!macro UserEnumRegKeyCall _Result _RegKeyPath _Count
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	Push "${_Count}"
	${CallArtificialFunction} UserEnumRegKey_
	Pop "${_Result}"
	!verbose pop
!macroend

!define UserEnumRegKey `!insertmacro UserEnumRegKeyCall`
!define un.UserEnumRegKey `!insertmacro UserEnumRegKeyCall`

!macro UserEnumRegKey
!macroend

!macro un.UserEnumRegKey
!macroend

!macro UserEnumRegKey_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R1				; _Count
	Exch
	Exch $R0				; _RegKeyPath
	Exch
;MessageBox MB_OK "UserEnumRegKey $HKCU_ID[$R0] [$R1]"
	${if} "$HKCU_ID" == ""
		EnumRegKey $R0 HKEY_CURRENT_USER "$R0" $R1
	${else}
		EnumRegKey $R0 HKEY_USERS "$HKCU_ID\$R0" $R1
	${endif}
;MessageBox MB_OK "UserEnumRegKey [$R3]"
	Pop $R1
	Exch $R0
	
	!verbose pop
!macroend



;
; Function UserEnumRegValue
; Usage ${UserEnumRegValue} $Result "reg path" "count"
;
!macro UserEnumRegValueCall _Result _RegKeyPath _Count
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	Push "${_Count}"
	${CallArtificialFunction} UserEnumRegValue_
	Pop "${_Result}"
	!verbose pop
!macroend

!define UserEnumRegValue `!insertmacro UserEnumRegValueCall`
!define un.UserEnumRegValue `!insertmacro UserEnumRegValueCall`

!macro UserEnumRegValue
!macroend

!macro un.UserEnumRegValue
!macroend

!macro UserEnumRegValue_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R1				; _Count
	Exch
	Exch $R0				; _RegKeyPath
	Exch
;MessageBox MB_OK "UserEnumRegValue $HKCU_ID[$R0] [$R1]"
	${if} "$HKCU_ID" == ""
		EnumRegValue $R0 HKEY_CURRENT_USER "$R0" $R1
	${else}
		EnumRegValue $R0 HKEY_USERS "$HKCU_ID\$R0" $R1
	${endif}
;MessageBox MB_OK "UserEnumRegValue [$R3]"
	Pop $R1
	Exch $R0
	
	!verbose pop
!macroend



;
; Function UserDeleteRegKey
; Usage ${UserDeleteRegKey} "reg path"
;
!macro UserDeleteRegKeyCall _RegKeyPath
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	${CallArtificialFunction} UserDeleteRegKey_
	!verbose pop
!macroend

!define UserDeleteRegKey `!insertmacro UserDeleteRegKeyCall`
!define un.UserDeleteRegKey `!insertmacro UserDeleteRegKeyCall`

!macro UserDeleteRegKey
!macroend

!macro un.UserDeleteRegKey
!macroend

!macro UserDeleteRegKey_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R0
	${if} "$HKCU_ID" == ""
		DeleteRegKey HKEY_CURRENT_USER "$R0"
	${else}
		DeleteRegKey HKEY_USERS "$HKCU_ID\$R0"
	${endif}
;MessageBox MB_OK "UserDeleteRegKey $HKCU_ID[$R0]"
	Exch $R0
	
	!verbose pop
!macroend



;
; Function UserDeleteRegKeyIfEmpty
; Usage ${UserDeleteRegKeyIfEmpty} "reg path"
;
!macro UserDeleteRegKeyIfEmptyCall _RegKeyPath
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	${CallArtificialFunction} UserDeleteRegKeyIfEmpty_
	!verbose pop
!macroend

!define UserDeleteRegKeyIfEmpty `!insertmacro UserDeleteRegKeyIfEmptyCall`
!define un.UserDeleteRegKeyIfEmpty `!insertmacro UserDeleteRegKeyIfEmptyCall`

!macro UserDeleteRegKeyIfEmpty
!macroend

!macro un.UserDeleteRegKeyIfEmpty
!macroend

!macro UserDeleteRegKeyIfEmpty_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R0
;MessageBox MB_OK "UserDeleteRegKeyIfEmpty $HKCU_ID[$R0]"
	${if} "$HKCU_ID" == ""
		DeleteRegKey /ifempty HKEY_CURRENT_USER "$R0"
	${else}
		DeleteRegKey /ifempty HKEY_USERS "$HKCU_ID\$R0"
	${endif}
	Exch $R0
	
	!verbose pop
!macroend



;
; Function UserDeleteRegValue
; Usage ${UserDeleteRegValue} "reg path" "value name"
;
!macro UserDeleteRegValueCall _RegKeyPath _ValueName
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	Push "${_ValueName}"
	${CallArtificialFunction} UserDeleteRegValue_
	!verbose pop
!macroend

!define UserDeleteRegValue `!insertmacro UserDeleteRegValueCall`
!define un.UserDeleteRegValue `!insertmacro UserDeleteRegValueCall`

!macro UserDeleteRegValue
!macroend

!macro un.UserDeleteRegValue
!macroend

!macro UserDeleteRegValue_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R1				; _ValueName
	Exch
	Exch $R0				; _RegKeyPath
	Exch
;MessageBox MB_OK "UserDeleteRegValue $HKCU_ID [$R0] [$R1]"
	${if} "$HKCU_ID" == ""
		DeleteRegValue HKEY_CURRENT_USER "$R0" "$R1"
	${else}
		DeleteRegValue HKEY_USERS "$HKCU_ID\$R0" "$R1"
	${endif}
	
	Pop $R1
	Pop $R0
	
	!verbose pop
!macroend



;
; Function UserReadRegStr
; Usage ${UserReadRegStr} $Result "reg path" "value name"
;
!macro UserReadRegStrCall _Result _RegKeyPath _ValueName
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	Push "${_ValueName}"
	${CallArtificialFunction} UserReadRegStr_
	Pop ${_Result}
	!verbose pop
!macroend

!define UserReadRegStr `!insertmacro UserReadRegStrCall`
!define un.UserReadRegStr `!insertmacro UserReadRegStrCall`

!macro UserReadRegStr
!macroend

!macro un.UserReadRegStr
!macroend

!macro UserReadRegStr_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R1				; _ValueName
	Exch
	Exch $R0				; _RegKeyPath
	Exch
;MessageBox MB_OK "UserReadRegStr $HKCU_ID [$R0] [$R1]"
	${if} "$HKCU_ID" == ""
		ReadRegStr $R0 HKEY_CURRENT_USER "$R0" "$R1"
	${else}
		ReadRegStr $R0 HKEY_USERS "$HKCU_ID\$R0" "$R1"
	${endif}
;MessageBox MB_OK "UserReadRegStr $R1=[$R0]"
	Pop $R1
	Exch $R0
	
	!verbose pop
!macroend



;
; Function UserReadRegDWORD
; Usage ${UserReadRegDWORD} $Result "reg path" "value name"
;
!macro UserReadRegDWORDCall _Result _RegKeyPath _ValueName
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	Push "${_ValueName}"
	${CallArtificialFunction} UserReadRegDWORD_
	Pop ${_Result}
	!verbose pop
!macroend

!define UserReadRegDWORD `!insertmacro UserReadRegDWORDCall`
!define un.UserReadRegDWORD `!insertmacro UserReadRegDWORDCall`

!macro UserReadRegDWORD
!macroend

!macro un.UserReadRegDWORD
!macroend

!macro UserReadRegDWORD_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R1				; _ValueName
	Exch
	Exch $R0				; _RegKeyPath
	Exch
;MessageBox MB_OK "UserReadRegDWORD $HKCU_ID [$R0] [$R1]"
	${if} "$HKCU_ID" == ""
		ReadRegDWORD $R0 HKEY_CURRENT_USER "$R0" "$R1"
	${else}
		ReadRegDWORD $R0 HKEY_USERS "$HKCU_ID\$R0" "$R1"
	${endif}
;MessageBox MB_OK "UserReadRegDWORD $R1=[$R0]"
	Pop $R1
	Exch $R0
	
	!verbose pop
!macroend



;
; Function UserWriteRegStr
; Usage ${UserWriteRegStr} "reg path" "value name" "value"
;
!macro UserWriteRegStrCall _RegKeyPath _ValueName _Value
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push `${_RegKeyPath}`
	Push `${_ValueName}`
	Push `${_Value}`
	${CallArtificialFunction} UserWriteRegStr_
	!verbose pop
!macroend

!define UserWriteRegStr `!insertmacro UserWriteRegStrCall`
!define un.UserWriteRegStr `!insertmacro UserWriteRegStrCall`

!macro UserWriteRegStr
!macroend

!macro un.UserWriteRegStr
!macroend

!macro UserWriteRegStr_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R2				; _Value
	Exch
	Exch $R1				; _ValueName
	Exch
	Exch 2
	Exch $R0				; _RegKeyPath
	Exch 2
;MessageBox MB_OK "UserReadRegDWORD $HKCU_ID [$R0] [$R1] [$R2]"
	${if} "$HKCU_ID" == ""
		WriteRegStr HKEY_CURRENT_USER "$R0" "$R1" "$R2"
	${else}
		WriteRegStr HKEY_USERS "$HKCU_ID\$R0" "$R1" "$R2"
	${endif}
;MessageBox MB_OK "UserReadRegDWORD $R1=[$R3]"
	Pop $R2
	Pop $R1
	Pop $R0
	
	!verbose pop
!macroend



;
; Function UserWriteRegDWORD
; Usage ${UserWriteRegDWORD} "reg path" "value name" "value"
;
!macro UserWriteRegDWORDCall _RegKeyPath _ValueName _Value
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}
	Push "${_RegKeyPath}"
	Push "${_ValueName}"
	Push "${_Value}"
	${CallArtificialFunction} UserWriteRegDWORD_
	!verbose pop
!macroend

!define UserWriteRegDWORD `!insertmacro UserWriteRegDWORDCall`
!define un.UserWriteRegDWORD `!insertmacro UserWriteRegDWORDCall`

!macro UserWriteRegDWORD
!macroend

!macro un.UserWriteRegDWORD
!macroend

!macro UserWriteRegDWORD_
	!verbose push
	!verbose ${_FILEFUNC_VERBOSE}

	Exch $R2				; _Value
	Exch
	Exch $R1				; _ValueName
	Exch
	Exch 2
	Exch $R0				; _RegKeyPath
	Exch 2
;MessageBox MB_OK "UserWriteRegDWORD $HKCU_ID [$R0] [$R1] [$R2]"
	${if} "$HKCU_ID" == ""
		WriteRegDWORD HKEY_CURRENT_USER "$R0" "$R1" "$R2"
	${else}
		WriteRegDWORD HKEY_USERS "$HKCU_ID\$R0" "$R1" "$R2"
	${endif}
;MessageBox MB_OK "UserWriteRegDWORD $R1=[$R3]"
	Pop $R2
	Pop $R1
	Pop $R0
	
	!verbose pop
!macroend



# [End of file dtxDefs.nsh]