/* ---------------------------------------------------------------------------------------
 * 
 * DTX Toolbar Installer
 * Copyright (c) 2010 Visicom Media, Inc.
 * 
 * dtxFirefox.nsh - FF-related functions
 * 
 * ---------------------------------------------------------------------------------------*/

;Constants for Addon Enabling notification code
!define FF_REGSITRATION_LINE1 "component {FB98835C-1B4B-4064-8F35-0A57C22D055C} msservice.js"
!define FF_REGSITRATION_LINE2 "contract @com.mystart.www/msservice;1 {FB98835C-1B4B-4064-8F35-0A57C22D055C}"
!define FF_REGSITRATION_LINE3 "category profile-after-change MSService @com.mystart.www/msservice;1"

!define FirefoxMinVersion	"3.6"

Var FirefoxPresent			; 1 = present, 0 - absent
Var FirefoxVersion
Var FirefoxInstallFolder	; %programfiles%\...

Var FirefoxProfDirs			; A list FF user profiles folders (semicolon-separated). 
							; ATTENTION! You should *never* use this variable for file operations directly, 
							; unless you implement something custom like ffEnumFolders, and ffEnumFolders could not meet your requirements.

Var FirefoxBaseFolder		; Receives *one* of the FF user profile folders *at a time* in a callback function called from ffEnumFolders
							; Use this variable in your callback function to work with a FF user profile folder. 


Var ffEnumFoldersCounter
Var ffEnumCallbackAddress
Var pluginCopied
!macro ffEnumFoldersCall _FUNC
	${If} $FirefoxPresent != 0
		!verbose push
		!verbose 4
		
		Push $1
		Push $2

		GetFunctionAddress $ffEnumCallbackAddress `${_FUNC}`				; Get user function address
		StrCpy $ffEnumFoldersCounter 0										; Init loop counter
		ffEnumFoldersCall_LOOP:
			; Extract next path from the string
			!ifdef __UNINSTALL__
				${UnStrTok} $1 $FirefoxProfDirs ";" $ffEnumFoldersCounter "1"
			!else
				${StrTok} $1 $FirefoxProfDirs ";" $ffEnumFoldersCounter "1"
			!endif
;messagebox mb_ok "1: [$1]"
			StrCmp $1 "" ffEnumFoldersCall_DONE			; If no more paths available then exit the loop, else continue
			IntOp $ffEnumFoldersCounter $ffEnumFoldersCounter + 1								; Increment loop counter

			${DirState} "$1" $2
;messagebox mb_ok "2: [$2]"
			StrCmp $2 "-1" ffEnumFoldersCall_LOOP 0		; If the pathname does not exist then start a new iteration, else continue

			StrCpy $FirefoxBaseFolder $1
;messagebox mb_ok "fbf: [$FirefoxBaseFolder]"

			Call $ffEnumCallbackAddress									; Call the user function
			Goto ffEnumFoldersCall_LOOP
		ffEnumFoldersCall_DONE:

		Pop $2
		Pop $1
		!verbose pop
	${endif}
!macroend	; ffEnumFoldersCall

!define ffEnumFolders		`!insertmacro ffEnumFoldersCall`
!define un.ffEnumFolders	`!insertmacro ffEnumFoldersCall`

!macro ffEnumFolders
!macroend

!macro un.ffEnumFolders
!macroend


!macro checkFirefoxVersion UN
Function ${UN}checkFirefoxVersion
	ReadRegStr $FirefoxVersion HKLM "Software\Mozilla\Mozilla Firefox" "CurrentVersion"
	${VersionCompare} "${FirefoxMinVersion}" "$FirefoxVersion" $R2
	${If} "$R2" == 1
		StrCpy $FirefoxPresent 0
	${Else}
		StrCpy $FirefoxPresent 1
		ReadRegStr $FirefoxInstallFolder HKLM "Software\Mozilla\Mozilla Firefox\$FirefoxVersion\Main" "Install Directory"
		StrCpy $FirefoxInstallFolder "$FirefoxInstallFolder\"
	${EndIf}
	
LogEx::Write "FirefoxPresent: $FirefoxPresent, Version: $FirefoxVersion, FirefoxInstallFolder: $FirefoxInstallFolder"
;MessageBox MB_OK "FirefoxPresent: $FirefoxPresent, Version: $FirefoxVersion, FirefoxInstallFolder: $FirefoxInstallFolder"
FunctionEnd
!macroend
!insertmacro checkFirefoxVersion ""
!insertmacro checkFirefoxVersion "un."






!macro getFirefoxProfileFolders UN
Function ${UN}getFirefoxProfileFolders
	Push $0
	Push $1
	Push $2
	Push $R0	; holds the part of the path *preceding* the UserName (because it can be localized!)
	Push $R1	; holds the part of the path *following* the UserName (because it can be localized!)
	Push $R2	; holds the full path to a FF folder for a particular user
	Push $R9	; holds a handle value for FindFirst/FindNext/FindClose 

	StrCpy $FirefoxProfDirs ""
	;StrCpy $FirefoxPresent 0	; Assume FF is not installed or its version is less than 3.

	;
	; Reading only FF CurrentVersion from Registry is not enough, because there could be a FF2 installation reporting 3.0.x in CurrentVersion.
	; Possibly this could happen when FF2 was installed over FF3, or FF3 was uninstalled while FF2 was kept.
	;
	ReadRegStr $0 HKLM "SOFTWARE\Mozilla\Mozilla Firefox" "CurrentVersion"
	;${StrFilter} $0 "31" "." "()-" $0
	;${VersionCompare} $0 ${FirefoxMinVersion} $0
	;StrCmp $0 2 done	; The required FirefoxMinVersion is greater than the installed one. Do not install the toolbar for FF, exiting.

	ReadRegStr $1 HKLM "SOFTWARE\Mozilla\Mozilla Firefox\$0\Main" "PathToExe"

	/*
	***********************************************************
	*   					!IMPORTANT!						  *
	***********************************************************
	;The code that sets $FirefoxPresent variable has been moved to CheckMinimumVersions in dtxUtils.nsh
	
	${If} $1 == ""		; The FF version number found in CurrentVersion [$0] doesn't have a corresponding entry in Registry, exiting.
		;messagebox MB_OK "The FF version number found in CurrentVersion [$0] doesn't have a corresponding entry in Registry"
		Goto done
	${EndIf}

	${WordFind} "$0" "." "+1" $0
	${If} $0 < 3
		Goto done
	${EndIf}
	; A good FF installation has been found, the toolbar can be installed for FF!
	StrCpy $FirefoxPresent 1
	*/	


	;
	; Retrieve the user profile folder path components (before and after the %UserName%)
	;
	; WinXP - C:\Documents and Settings\%USERNAME%\Application Data
	; Vista - C:\Users\%USERNAME%\AppData\Roaming
	;
	ExpandEnvStrings $0 "%USERPROFILE%"
	${WordFind} "$0" "\" "-2{*" $R0							; $R0	- Receives the part of the path preceding the UserName (because it can be localized!)
	;${WordFind} "$PROFILE" "\" "-2{*" $R0					; $R0	- Receives the part of the path preceding the UserName (because it can be localized!)
	${WordFind} "$APPDATA" "\" "+3}" $R1					; $R1	- Receives the part of the path following the UserName (because it can be localized!)
;messagebox MB_OK "Full Path = $APPDATA$\nUser Folder = $R0$\nAppData = $R2"

LogEx::Write "	> Base profile folder on this computer (username excluded): $R0"
LogEx::Write "	> AppData folder (the part after username): $R1"
LogEx::Write "+ Enum user profile folders - begin"

	FindFirst $R9 $1 $R0\*.*								; $1	- Receives folder name
	loop_UserFolders:
;messagebox MB_OK "Folder = [$1]"
		StrCmp $1 "" done_UserFolders						; No more folders left in the directory, exiting.
		StrCmp $1 "." continue_UserFolders
		StrCmp $1 ".." continue_UserFolders

LogEx::Write "	User folder name: $1"
		;
		; WinXP - C:\Documents and Settings\%USERNAME%\Application Data\Mozilla\Firefox\Profiles
		; Vista - C:\Users\%USERNAME%\AppData\Roaming\Mozilla\Firefox\Profiles
		;
		StrCpy $R2 "$R0\$1\$R1\Mozilla\Firefox"				; $R2	- Path to FF profiles folder
;messagebox MB_OK "FF = $R1"
LogEx::Write "	Firefox profile folder: $R2"

		IfFileExists "$R2\profiles.ini" 0 continue_UserFolders
		;${DirState} $R1 $1
		;StrCmp $1 -1 continue_UserFolders
		
LogEx::Write "	* Firefox: profiles.ini FOUND, parsing the file - begin"

;messagebox MB_OK "profiles.ini EXISTS:$\n$R1"

		StrCpy $1 0											; $1	- User Profile Count in profiles.ini, starting from 0
		prof_loop:
			ReadINIStr $0 $R2\profiles.ini "Profile$1" Path
			StrCmp $0 "" prof_loop_end 0

			;${WordReplace} $0 "/" "\" "+" $0				; ???!!! This creates an endless loop: it always does an unconditional jump to loop_UserFolders
			Push $0
			Push "/"
			Call ${UN}StrSlash
			Pop $0

			ReadINIStr $2 $R2\profiles.ini "Profile$1" IsRelative
			StrCmp $2 "1" 0 +2								; If the Path is relative, add the parent dir:
			StrCpy $0 "$R2\$0"
			StrCmp $FirefoxProfDirs "" +2 0					; If FirefoxProfDirs is not empty, append a separator (;)
			StrCpy $FirefoxProfDirs "$FirefoxProfDirs;"
			StrCpy $FirefoxProfDirs "$FirefoxProfDirs$0"
			IntOp $1 $1 + 1
LogEx::Write "		Firefox user profiles folder path: $0"
			Goto prof_loop
		prof_loop_end:
LogEx::Write "	* Firefox: profiles.ini FOUND, parsing the file - end."

		continue_UserFolders:
			FindNext $R9 $1
;messagebox MB_OK "FindNext $R9 [$1]"
			;IfErrors done_UserFolders loop_UserFolders
			Goto loop_UserFolders
	done_UserFolders:
	FindClose $R9

LogEx::Write "+ Enum user profile folders - end."

;messagebox MB_OK "FOUND:$\n$FirefoxProfDirs"
;done:	
	Pop $R9
	Pop $R2
	Pop $R1
	Pop $R0
	Pop $2
	Pop $1
	Pop $0
FunctionEnd
!macroend
!insertmacro getFirefoxProfileFolders ""
!insertmacro getFirefoxProfileFolders "un."
